package com.exemple.profedam.calculadoradpi;


import android.content.pm.ActivityInfo;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.exemple.profedam.calculadoradpi.controllers.MainActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.IsNot.not;

@RunWith(AndroidJUnit4.class)
public class TestActivity {

    @Rule
            public ActivityTestRule<MainActivity> activityRule =
            new ActivityTestRule<>(MainActivity.class, true,
                    true);

    @Test
    public void VerticalErrorTest() {
        onView(withId(R.id.etResolucioVertical)).perform(typeText(""),closeSoftKeyboard());
        onView(withId(R.id.etResolucioHoritzontal)).perform(typeText("1776"),closeSoftKeyboard());
        onView(withId(R.id.etDiagonal)).perform(typeText("6.5"),closeSoftKeyboard());
        onView(withId(R.id.btnCalcular)).perform(click());
        onView(withText("Falta resolución vertical")).inRoot(withDecorView(not(activityRule.getActivity().getWindow().getDecorView()))).check(matches(isDisplayed()));
    }

    @Test
    public void HorizontalErrorTest() {
        onView(withId(R.id.etResolucioVertical)).perform(typeText("1080"),closeSoftKeyboard());
        onView(withId(R.id.etResolucioHoritzontal)).perform(typeText(""),closeSoftKeyboard());
        onView(withId(R.id.etDiagonal)).perform(typeText("6.5"),closeSoftKeyboard());
        onView(withId(R.id.btnCalcular)).perform(click());
        onView(withText("Falta resolución horizontal")).inRoot(withDecorView(not(activityRule.getActivity().getWindow().getDecorView()))).check(matches(isDisplayed()));
    }

    @Test
    public void DiagonalErrorTest() {
        onView(withId(R.id.etResolucioVertical)).perform(typeText("1080"),closeSoftKeyboard());
        onView(withId(R.id.etResolucioHoritzontal)).perform(typeText("1776"),closeSoftKeyboard());
        onView(withId(R.id.etDiagonal)).perform(typeText(""),closeSoftKeyboard());
        onView(withId(R.id.btnCalcular)).perform(click());
        onView(withText("Falta Diagonal")).inRoot(withDecorView(not(activityRule.getActivity().getWindow().getDecorView()))).check(matches(isDisplayed()));
    }

    @Test
    public void SuccessTest() {

        onView(withId(R.id.etResolucioVertical)).perform(typeText("1080"),closeSoftKeyboard());
        onView(withId(R.id.etResolucioHoritzontal)).perform(typeText("1776"),closeSoftKeyboard());
        onView(withId(R.id.etDiagonal)).perform(typeText("6.5"),closeSoftKeyboard());
        onView(withId(R.id.btnCalcular)).perform(click());
        onView(withId(R.id.tvDpi)).check(matches(withText("319 dpi")));
    }

    @Test
    public void SuccessTestHorizontal() {
        activityRule.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        onView(withId(R.id.tvDiagonalED)).check(matches(withText("4.3")));

    }


}
