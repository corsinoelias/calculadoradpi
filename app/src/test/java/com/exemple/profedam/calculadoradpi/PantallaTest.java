package com.exemple.profedam.calculadoradpi;


import com.exemple.profedam.calculadoradpi.model.Pantalla;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class PantallaTest {

    @Test
    public void testPantallaDiagonal(){

        Pantalla pantalla = new Pantalla(380,520,3.4);
        int expDpi = 189;
        assertEquals(expDpi, pantalla.getDpi());
    }

    @Test
    public void testPantallaDpi(){

        Pantalla pantalla = new Pantalla(380, 520, 189);
        double exDiagonal = 3.4;
        assertEquals(exDiagonal, pantalla.getDiagonal(),0.0);
    }
}

