package com.exemple.profedam.calculadoradpi;

import com.exemple.profedam.calculadoradpi.utils.MyUtils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MyUtilsTest {

    @Test
    public void testHipotenusa(){

        assertEquals(5 , MyUtils.calcularHipotenusa(3.0,4.0),0.0);
        assertEquals(1.414,MyUtils.calcularHipotenusa(1,1),0.001);
    }

    @Test
    public void testString(){

        String test = "";
        String empty = null;

        assertTrue(MyUtils.stringIsNullOrEmpty(test));
        assertTrue(MyUtils.stringIsNullOrEmpty(empty));
        assertFalse(MyUtils.stringIsNullOrEmpty("hola"));
    }

    @Test
    public void testRound(){

        assertEquals(2.2,MyUtils.round(2.19,1),0.0);
    }
}
